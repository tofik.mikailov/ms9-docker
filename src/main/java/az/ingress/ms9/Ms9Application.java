package az.ingress.ms9;

import az.ingress.ms9.model.Account;
import az.ingress.ms9.model.Course;
import az.ingress.ms9.model.Student;
import az.ingress.ms9.repository.AccountRepository;
import az.ingress.ms9.repository.CourseRepository;
import az.ingress.ms9.service.AccountService;
import az.ingress.ms9.service.TransferService;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.Transactional;

@EnableCaching
@SpringBootApplication
@EnableConfigurationProperties
@RequiredArgsConstructor
public class Ms9Application implements CommandLineRunner {

    private final AccountService accountService;
    private final TransferService transferService;
    private final AccountRepository accountRepository;
    private final CourseRepository courseRepository;

    public static void main(String[] args) {
        SpringApplication.run(Ms9Application.class, args);
    }

    @Override
//    @Transactional
    public void run(String... args) throws Exception {
//        Account account = new Account();
//        account.setName("Azer");
//        account.setBalance(200.0);
//
//        accountRepository.save(account);
    }
}
