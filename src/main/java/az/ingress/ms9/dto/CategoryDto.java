package az.ingress.ms9.dto;

import az.ingress.ms9.model.Category;
import az.ingress.ms9.model.CategoryType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CategoryDto {

    Long id;
    CategoryType name;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o instanceof Category) {
            Category category = (Category) o;
            return this.name.equals(category.getName());
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CategoryDto that = (CategoryDto) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        return name == that.name;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
