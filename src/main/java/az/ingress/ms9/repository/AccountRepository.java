package az.ingress.ms9.repository;

import az.ingress.ms9.model.Account;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {

    //    @Lock(LockModeType.OPTIMISTIC)
    Optional<Account> findById(Long id);
}
