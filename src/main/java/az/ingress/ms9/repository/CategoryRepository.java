package az.ingress.ms9.repository;

import az.ingress.ms9.model.Category;
import az.ingress.ms9.model.CategoryType;
import az.ingress.ms9.model.Product;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    Optional<Category> findByName(CategoryType categoryType);
}
