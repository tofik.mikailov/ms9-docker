package az.ingress.ms9.repository;

import az.ingress.ms9.model.Car;
import az.ingress.ms9.model.Product;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {

}
