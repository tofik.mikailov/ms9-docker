package az.ingress.ms9.repository;

import az.ingress.ms9.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query(nativeQuery = true, value = "select * from student where student.id= :id")
    Student getByID(Long id);
}
