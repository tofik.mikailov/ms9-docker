package az.ingress.ms9.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import az.ingress.ms9.dto.ManufacturerDto;
import az.ingress.ms9.model.Manufacturer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface ManufactureMapper {

}
