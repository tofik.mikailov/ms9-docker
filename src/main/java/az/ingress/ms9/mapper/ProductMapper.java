package az.ingress.ms9.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import az.ingress.ms9.dto.CategoryDto;
import az.ingress.ms9.dto.ManufacturerDto;
import az.ingress.ms9.dto.ProductDto;
import az.ingress.ms9.model.Category;
import az.ingress.ms9.model.Manufacturer;
import az.ingress.ms9.model.Product;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface ProductMapper {

    Product mapDtoToEntity(ProductDto dto);

    ProductDto entityToDto(Product product);

    Manufacturer dtoToEntity(ManufacturerDto dto);

    Category categoryDtoToEntity(CategoryDto categoryDto);

    List<Category> listToEntity(List<CategoryDto> dtoList);
}
