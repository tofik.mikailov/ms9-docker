package az.ingress.ms9.controller;

import az.ingress.ms9.model.Account;
import az.ingress.ms9.repository.AccountRepository;
import az.ingress.ms9.service.AccountService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class Controller {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;
    private final AccountService accountService;

    @GetMapping("/fast")
    public Account getStudent() {
        Account account = new Account();
        account.setName("Azer");
        return account;
    }

    @GetMapping("/slow")
    public Mono<Account> getStudent2() throws InterruptedException {
        return Mono.just(get(1L));
    }


    private Account get(Long id) throws InterruptedException {
        Account account = new Account();
        account.setName("Azer");
        Thread.sleep(1000);
        return account;
    }
}
