package az.ingress.ms9.controller;

import az.ingress.ms9.dto.ProductDto;
import az.ingress.ms9.service.ProductService;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("/list")
    public Set<ProductDto> getAllProducts() {
        return productService.getAllProducts();
    }

    @PostMapping
    public ProductDto save(@RequestBody ProductDto dto) {
        return productService.save(dto);
    }

    @GetMapping
    public List<ProductDto> getAllProductsByPriceOver(@RequestParam Double price) {
        return productService.getAllProductsByPriceOver(price);
    }
}
