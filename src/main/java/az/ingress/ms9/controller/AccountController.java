package az.ingress.ms9.controller;

import az.ingress.ms9.model.Account;
import az.ingress.ms9.service.AccountService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/{id}/sleep")
    public Account getAccountByIdWithSleep(@PathVariable Long id) {
        return accountService.getByIdWithSleep(id);
    }

    @GetMapping("/{id}")
    public Account getAccountById(@PathVariable Long id) {
        return accountService.getById(id);
    }

    @GetMapping("/{id}/pass")
    public String getAccountPassword(@PathVariable Long id) {
        return accountService.getPassword(id);
    }

    @GetMapping("/all")
    public List<Account> getAll() {
        return accountService.getAll();
    }

    @PutMapping("/{id}")
    public Account getAccountById(@PathVariable Long id, @RequestBody Account account) throws InterruptedException{
        return accountService.update(id, account);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        accountService.delete(id);
    }

}
