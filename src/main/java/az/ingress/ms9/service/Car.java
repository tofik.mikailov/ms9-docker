package az.ingress.ms9.service;

public class Car {

    private Integer id;

    public Car(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Car is destroyed");
    }
}
