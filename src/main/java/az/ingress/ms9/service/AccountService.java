package az.ingress.ms9.service;

import az.ingress.ms9.model.Account;
import az.ingress.ms9.repository.AccountRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService {

    private final AccountRepository accountRepository;
    private final CacheManager cacheManager;

//    @Transactional
    @SneakyThrows
    public Account getById(Long id) {
//        Account account = accountRepository.findById(id).get();
        Account account = new Account();
        account.setName("Azer");
        Thread.sleep(100);
        return account;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @SneakyThrows
    public Account getByIdWithSleep(Long id) {
        Account account = accountRepository.findById(id).get();
        Thread.sleep(5000);
        return account;
    }

    public Account update(Long id, Account account) throws InterruptedException {
        try {
            Account account1 = accountRepository.findById(id).get();
            //id 7 , version 5
            Thread.sleep(5000);
            account1.setBalance(account.getBalance());
            accountRepository.save(account1);
            return account1;
        } catch (HibernateException e) {
            log.error("there is a conflict");
            return update(id, account);
        }
    }

    public String getPassword(Long id) {
        Cache cache = cacheManager.getCache("userSecretPassword");
        String userSecretPassword = cache.get(id, String.class);
        cache.evict(id);

        return userSecretPassword;
    }

    @CacheEvict(cacheNames = "accounts", allEntries = true)
    public void delete(Long id) {
        accountRepository.deleteById(id);
    }

    //    @Cacheable(cacheNames = "AllAccounts", key = "#root.method.name")
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    public void stream() {
        List<Long> integers = List.of(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8l, 10L);
        List<Optional<Account>> collect =
                integers.parallelStream().map(i -> {
                    System.out.println("gettind from account with account id : " + i);
                    Optional<Account> byId = accountRepository.findById(i);
                    return byId;
                }).collect(Collectors.toList());
    }

}
