package az.ingress.ms9.service;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

public class Main {

    public static void main(String[] args) {
        Car car = new Car(1);
        ReferenceQueue<Car> rf = new ReferenceQueue<>();
        PhantomReference<Car> weakReference = new PhantomReference<>(car, rf);
        car = null;
        System.out.println("Before gc id is: " + weakReference.get());
        System.gc();

        System.out.println("After gc id is: " + weakReference.get());
    }
}
