package az.ingress.ms9.service;

import az.ingress.ms9.model.Account;
import az.ingress.ms9.repository.AccountRepository;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.OptimisticLockException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransferService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;

    Map<Object, Account> l1Cache = new HashMap<>();

    private EntityManager entityManager;

    public void l1CacheSample() {
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Account one = entityManager.find(Account.class, 8L);
        l1Cache.put(8L, one);

        Account two;
        if (l1Cache.containsKey(8L)) {
            two = l1Cache.get(8L);
        } else {
            two = entityManager.find(Account.class, 8L);
        }

        Account three = entityManager.find(Account.class, 8L);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void detachedSample() {
//        Account account = accountRepository.getById(7L);
        Optional<Account> account = accountRepository.findById(7L);
        System.out.println(account.get().getId());
    }

//    public void transferProxy(Double amount) throws Exception {
//        entityManager = entityManagerFactory.createEntityManager();
//        entityManager.getTransaction().begin();
//        Account one = entityManager.find(Account.class, 7L);
//        Account two = entityManager.find(Account.class, 8L);
//        try {
//            transfer(one, two, amount);
//        } catch (RuntimeException e) {
//            entityManager.getTransaction().rollback();
//            throw e;
//        } finally {
//            log.info("finally block");
//            entityManager.getTransaction().commit();
//            entityManager.close();
//        }
//    }

    @Transactional
    @SneakyThrows
    public void transfer(Double amount) {
        try {
            Account one = accountRepository.findById(7L).get();
            log.trace("Thread with id {} get account with id {}. Account data is : {}", Thread.currentThread().getId(),
                    7L,
                    one);
            if (one.getBalance() < amount) {
                throw new RuntimeException("Balans kifayet deyil");
            }
            Thread.sleep(10000);
            Account two = accountRepository.findById(8L).get();
            log.trace("Thread with id {} get account with id {}. Account data is : {}", Thread.currentThread().getId(),
                    8L,
                    two);
            one.setBalance(one.getBalance() - amount);
            two.setBalance(two.getBalance() + amount);
            log.info("transfer method end");
        } catch (OptimisticLockException e) {
            e.printStackTrace();
            transfer(amount);
        }
    }


    public void transferEntityManager(Double amount) throws Exception {
        System.out.println("SElect 1");
        Account account = accountRepository.findById(7L).get();
        System.out.println(account);

        System.out.println("SElect 2");
        Account account1 = accountRepository.findById(7L).get();
        System.out.println(account1);

        System.out.println("SElect 3");
        Account account2 = accountRepository.findById(7L).get();
        System.out.println(account2);

        System.out.println(account == account2);

//        entityManager = entityManagerFactory.createEntityManager();
//        entityManager.getTransaction().begin();

//        System.out.println("SElect 1");
//        Account one = entityManager.find(Account.class, 7L);
//        System.out.println("SElect 2");
//        Account one1 = entityManager.find(Account.class, 7L);
//
//
//        System.out.println("Transaction end");
//        entityManager.getTransaction().commit();
//
//        System.out.println("SElect 3");
//        Account one3 = entityManager.find(Account.class, 7L);
//
//        entityManager.close();
//
//        System.out.println("SElect 4");
//        entityManager = entityManagerFactory.createEntityManager();
//        entityManager.getTransaction().begin();
//        Account one4 = entityManager.find(Account.class, 7L);
    }
}
