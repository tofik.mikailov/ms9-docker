package az.ingress.ms9.service;

import az.ingress.ms9.dto.CategoryDto;
import az.ingress.ms9.dto.ProductDto;
import az.ingress.ms9.mapper.ManufactureMapper;
import az.ingress.ms9.mapper.ProductMapper;
import az.ingress.ms9.model.Category;
import az.ingress.ms9.model.Product;
import az.ingress.ms9.repository.CategoryRepository;
import az.ingress.ms9.repository.ProductRepository;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ModelMapper modelMapper = new ModelMapper();
    private final ProductMapper productMapper;

    @Override
    public Set<ProductDto> getAllProducts() {
        Set<ProductDto> collect = productRepository.findAll().stream().map(product -> {
//            ProductDto productDto = modelMapper.map(product, ProductDto.class);
            ProductDto productDto = productMapper.entityToDto(product);
            return productDto;
        }).collect(Collectors.toSet());
        return collect;
    }

    @Override
    public ProductDto save(ProductDto dto) {
//        List<Category> all = categoryRepository.findAll();
//        all.stream().filter(category -> category.equals())
        Set<Category> collect = dto.getCategoryDto().stream().map(categoryDto ->
                categoryRepository.findByName(categoryDto.getName()).orElse(Category.builder()
                        .name(categoryDto.getName())
                        .build())
        ).collect(Collectors.toSet());


        Product product = modelMapper.map(dto, Product.class);
//        Manufacturer manufacturer = modelMapper.map(dto.getManufacturerDto(), Manufacturer.class);
//        product.setManufacturer(manufacturer);
        Set<Category> categories = dto.getCategoryDto().stream().map(categoryDto -> {
            Category category = modelMapper.map(categoryDto, Category.class);
            return category;
        }).collect(Collectors.toSet());
        product.setCategories(collect);
        productRepository.save(product);
        return dto;
    }

    @Override
    public List<ProductDto> getAllProductsByPriceOver(Double price) {
        List<Product> allByPriceGreaterThan = productRepository.findAllByPriceGreaterThan(BigDecimal.valueOf(price));

        List<ProductDto> collect = allByPriceGreaterThan.stream().map(product -> {
            ProductDto productDto = modelMapper.map(product, ProductDto.class);
            return productDto;
        }).collect(Collectors.toList());
        return collect;
    }
}
