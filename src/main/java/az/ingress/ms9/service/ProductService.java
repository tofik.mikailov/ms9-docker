package az.ingress.ms9.service;

import az.ingress.ms9.dto.ProductDto;
import java.util.List;
import java.util.Set;

public interface ProductService {
    Set<ProductDto> getAllProducts();

    ProductDto save(ProductDto dto);

    List<ProductDto> getAllProductsByPriceOver(Double price);
}
