FROM openjdk:17-alpine
COPY build/libs/ms9-0.0.1-SNAPSHOT.jar /app/
CMD ["java", "-jar", "/app/ms9-0.0.1-SNAPSHOT.jar"]